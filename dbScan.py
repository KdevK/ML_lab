import time

import pygame
import numpy as np
import random
import seaborn as sns


def get_distance(pointA, pointB):
    return np.sqrt(
        (pointA[0] - pointB[0]) * (pointA[0] - pointB[0]) + (pointA[1] - pointB[1]) * (pointA[1] - pointB[1]))


def generate_random_points(coord):
    count = random.randint(2, 5)
    points = []
    for i in range(count):
        angle = np.pi * random.randint(0, 360) / 180
        radius = random.randint(10, 20)
        x = radius * np.cos(angle) + coord[0]
        y = radius * np.sin(angle) + coord[1]
        points.append((x, y))
    return points


def find_neighbors(data, point_index, eps):
    neighbors = []
    for i, point in enumerate(data):
        if get_distance(data[point_index], point) <= eps:
            neighbors.append(i)
    return neighbors


def expand_cluster(data, labels, point_index, neighbors, cluster_label, eps, min_samples):
    labels[point_index] = cluster_label

    i = 0
    while i < len(neighbors):
        neighbor_index = neighbors[i]

        if labels[neighbor_index] == -1:
            labels[neighbor_index] = cluster_label
        elif labels[neighbor_index] == 0:
            labels[neighbor_index] = cluster_label
            neighbor_neighbors = find_neighbors(data, neighbor_index, eps)

            if len(neighbor_neighbors) >= min_samples:
                neighbors = neighbors + neighbor_neighbors

        i += 1


def dbscan(data, eps, min_samples, screen, points, colors):
    labels = [0] * len(data)
    cluster_label = 0

    for i, point in enumerate(data):
        if labels[i] != 0:
            continue

        neighbors = find_neighbors(data, i, eps)

        if len(neighbors) >= min_samples:
            colors[i] = '#00FF00'
    redraw_points(screen, points, colors)
    pygame.display.update()

    for i, point in enumerate(data):
        if labels[i] != 0:
            continue

        neighbors = find_neighbors(data, i, eps)

        if len(neighbors) < min_samples:
            colors[i] = '#FF0000'
            for j in neighbors:
                if colors[j] == '#00FF00':
                    colors[i] = '#FFFF00'
    redraw_points(screen, points, colors)
    pygame.display.update()
    time.sleep(1)
    for i, point in enumerate(data):
        if labels[i] != 0:
            continue

        neighbors = find_neighbors(data, i, eps)

        if len(neighbors) < min_samples:
            labels[i] = -1
        else:
            cluster_label += 1
            expand_cluster(data, labels, i, neighbors, cluster_label, eps, min_samples)

    return labels, cluster_label + 1


def redraw_points(screen, points, colors):
    screen.fill(color='#000000')
    for point, clr in zip(points, colors):
        pygame.draw.circle(screen, color=clr,
                           center=point, radius=r)


if __name__ == '__main__':
    r = 3
    pygame.init()
    screen = pygame.display.set_mode((600, 600), pygame.RESIZABLE)
    screen.fill(color='#000000')
    pygame.display.update()
    is_pressed = False
    points = []
    colors = []
    eps = 25
    min_samples = 3
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
            if event.type == pygame.VIDEORESIZE:
                redraw_points(screen, points, colors)
            if event.type == pygame.KEYUP:
                if event.key == 8:
                    screen.fill(color='#000000')
                    points = []
                    colors = []
                elif event.key == 13:
                    labels, labels_n = dbscan(points, eps, min_samples, screen, points, colors)

                    palette = sns.color_palette("Set1", n_colors=labels_n)
                    distinct_colors = list(palette.as_hex())
                    colors = [distinct_colors[label] for label in labels]
                    redraw_points(screen, points, colors)

            if event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1:
                    is_pressed = True
            if event.type == pygame.MOUSEBUTTONUP:
                is_pressed = False
            if is_pressed:
                coord = event.pos
                if len(points):
                    if get_distance(points[-1], coord) > 5 * r:
                        pygame.draw.circle(screen, color='#FF0000',
                                           center=coord, radius=r)
                        near_point = generate_random_points(coord)
                        clrs = ['#FF0000'] * len(near_point)
                        points.extend(near_point)
                        colors.extend(clrs)
                        for elem in near_point:
                            pygame.draw.circle(screen, color='#FF0000',
                                               center=elem, radius=r)
                        points.append(coord)
                        colors.append('#FF0000')
                else:
                    pygame.draw.circle(screen, color='#FF0000',
                                       center=coord, radius=r)
                    points.append(coord)
                    colors.append('#FF0000')

            pygame.display.update()
